var ip = '192.168.0.89:8089'


const tablaUsuarios = document.getElementById('tabla-usuarios').getElementsByTagName('tbody')[0];

fetch('http://' + ip + '/usuario')
	.then(response => response.json())
	.then(data => {
		data.forEach(usuario => {
			const fila = tablaUsuarios.insertRow();

			fila.insertCell().textContent = usuario.id;
			fila.insertCell().textContent = usuario.ci;
			fila.insertCell().textContent = "Pedro Perez Lopez";
			fila.insertCell().textContent = usuario.cel;

			//-----
			const button = document.createElement('button');
			button.classList.add('btn', 'btn-success', "icon-button", 'btn-margen');
			const icon = document.createElement('i');
			icon.classList.add('bi', 'bi-eye');
			button.appendChild(icon);

			fila.insertCell().appendChild(button);
			button.onclick = function () {
				ventanaSolicitud(usuario.id)
			};


			const button2 = document.createElement('button');
			button2.classList.add('btn', 'btn-danger', 'btn-margen');
			const icon2 = document.createElement('i');
			icon2.classList.add('bi', 'bi-trash');
			button2.appendChild(icon2);

			fila.insertCell().appendChild(button2);


			//----


		});
	})
	.catch(error => {
		console.error('Error al obtener los datos de los usuarios:', error);
	});

function ventanaSolicitud(contx) {
	document.getElementById('btn1').style.visibility = 'initial';
	bandera = 2;
	document.getElementById('tabla-usuarios').style.display = 'none';
	var elemento = document.getElementById('tabla-solicitud');
	elemento.style.display = '';
	var tabla = document.getElementById('tabla-solicitud').getElementsByTagName('tbody')[0];;
	fetch('http://' + ip + '/solicitud/listarUx/' + contx)
		.then(response => response.json())
		.then(data => {
			// Iterar sobre los datos obtenidos
			data.forEach(solicitud => {
				// Crear una nueva fila
				var fila = tabla.insertRow();

				fila.insertCell().textContent = "01/06/2023";
				fila.insertCell().textContent = "13:05";
				fila.insertCell().textContent = solicitud.id;
				fila.insertCell().textContent = solicitud.proposito;
				fila.insertCell().textContent = solicitud.monto;
				fila.insertCell().textContent = solicitud.moneda;
				fila.insertCell().textContent = solicitud.ingresoMensual;
				fila.insertCell().textContent = solicitud.tipoIngreso;
				fila.insertCell().textContent = solicitud.estadoCivil;


				const button = document.createElement('button');
				button.classList.add('btn', 'btn-success', 'btn-margen');
				const icon = document.createElement('i');
				icon.classList.add('bi', 'bi-eye');
				button.appendChild(icon);

				fila.insertCell().appendChild(button);

				button.onclick = function () {
					ventanaRespaldo(solicitud.id,solicitud.usuario.ci)
				};


				var radio1 = document.createElement("input");
				radio1.type = "radio";
				radio1.name = "opciones" + solicitud.id;
				fila.insertCell().appendChild(radio1);

				var radio2 = document.createElement("input");
				radio2.type = "radio";
				radio2.name = "opciones" + solicitud.id;
				fila.insertCell().appendChild(radio2);

				var radio3 = document.createElement("input");
				radio3.type = "radio";
				radio3.name = "opciones" + solicitud.id;

				fila.insertCell().appendChild(radio3);








			});
		})
		.catch(error => {
			console.error('Error al obtener los datos de la API:', error);
		});


}

function ventanaRespaldo(contx,cix) {
	console.log(cix);
	bandera = 3;
	document.getElementById('tabla-solicitud').style.display = 'none';
	var elemento = document.getElementById('tabla-respaldos');
	elemento.style.display = '';
	var tabla = document.getElementById('tabla-respaldos').getElementsByTagName('tbody')[0];;
	fetch('http://' + ip + '/respaldo/listarSx/' + contx)
		.then(response => response.json())
		.then(data => {
			// Iterar sobre los datos obtenidos
			data.forEach(respaldo => {
				// Crear una nueva fila
				var fila = tabla.insertRow();
				fila.insertCell().textContent = respaldo.id;
				fila.insertCell().textContent = respaldo.tipo;

				// imagen
				if ((respaldo.imagen + "").length > 15) {
					// var imgElement = document.createElement("img");
					// imgElement.src = "http://" + ip + "/admin/imagenes/" + respaldo.imagen;
					// // console.log("http://" + ip + "/admin/imagenes/" + respaldo.imagen);
					// imgElement.style.maxWidth = "200px"; // Establece el ancho máximo de la imagen
					// imgElement.style.maxHeight = "200px";
					// fila.insertCell().appendChild(imgElement);
					// otro
					// var imgElement = document.createElement("img");
					// imgElement.src = "http://" + ip + "/admin/imagenes/" + respaldo.imagen;
					// imgElement.style.maxWidth = "200px";
					// imgElement.style.maxHeight = "200px";

					// // Agregar evento de clic a la imagen
					// imgElement.addEventListener("click", function () {
					// 	// Modificar el estilo de la imagen al hacer clic
					// 	imgElement.style.maxWidth = "100%"; // Ampliar el ancho al máximo
					// 	imgElement.style.maxHeight = "100%"; // Ampliar el alto al máximo
					// });

					// fila.insertCell().appendChild(imgElement);
					// otro
					var imgElement = document.createElement("img");
					imgElement.src = "http://" + ip + "/admin/imagenes/" + respaldo.imagen;
					imgElement.style.maxWidth = "200px";
					imgElement.style.maxHeight = "200px";

					// Agregar evento de clic a la imagen
					imgElement.addEventListener("click", function () {
						// Mostrar el modal y establecer la imagen en el modal
						document.getElementById("imageModal").style.display = "block";
						document.getElementById("modalImage").src = imgElement.src;
					});

					// Agregar evento de clic al botón de cierre del modal
					document.getElementsByClassName("close")[0].addEventListener("click", function () {
						// Ocultar el modal al hacer clic en el botón de cierre
						document.getElementById("imageModal").style.display = "none";
					});

					// Agregar evento de clic a la imagen dentro del modal
					document.getElementById("modalImage").addEventListener("click", function () {
						// Ocultar el modal al hacer clic en la imagen
						document.getElementById("imageModal").style.display = "none";
					});

					fila.insertCell().appendChild(imgElement);




				} else {
					fila.insertCell().textContent = respaldo.imagen;
				}

				fila.insertCell().textContent = respaldo.ocr;

			});
		})
		.catch(error => {
			console.error('Error al obtener los datos de la API:', error);
		});


}
var bandera = 1;
function volver() {
	switch (bandera) {
		case 1:

			break;
		case 2:
			document.getElementById('btn1').style.visibility = 'hidden';
			document.getElementById('tabla-usuarios').style.display = '';
			document.getElementById('tabla-solicitud').style.display = 'none';
			bandera = 1
			break;
		case 3:
			document.getElementById('tabla-solicitud').style.display = '';
			document.getElementById('tabla-respaldos').style.display = 'none';
			bandera = 2
			break;
	}

}