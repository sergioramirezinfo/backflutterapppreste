package com.example.backPreste.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.backPreste.entity.respaldo;
import com.example.backPreste.entity.solicitud;
import com.example.backPreste.service.respaldoService;
import com.example.backPreste.service.solicitudService;

@RestController
@RequestMapping("/respaldo")
@CrossOrigin
public class respaldoController {
	@Autowired
	private respaldoService respaldoxService;
	
	
	@GetMapping
	public List<respaldo> listar(){
		return respaldoxService.listar();
	}
	
	@PostMapping
	public respaldo insertar(@RequestBody respaldo sol){
		return respaldoxService.insertar(sol);
	}
	
	@DeleteMapping
	public void eliminar(@RequestBody int id){
		respaldoxService.eliminar(id);
	}
	
	@Autowired
	private solicitudService solicitudxService;
	@RequestMapping(value = "listarSx/{usx}", method = RequestMethod.GET)
	public List<respaldo> listarrespaldoesUx(@PathVariable int usx) {
		solicitud solicitudx = new solicitud();
		solicitudx.setId(usx);
//	    usuario usuario = usuarioService.getUsuarioById(usx);
	    return respaldoxService.listarRespaldosSx(solicitudx);
	}
	
	

	// guardar imagenes
	
	// Ruta del directorio donde se guardarán las imágenes
    private static final String UPLOAD_DIR = "src/main/resources/static/admin/imagenes"; 
    
    @PostMapping("/img")
    public ResponseEntity<String> uploadImage(@RequestParam("image") MultipartFile image) {
    	
    	
    	String absolutePath = ".";
        String imagePath = absolutePath + File.separator + UPLOAD_DIR;

        File imageDirectory = new File(imagePath);
        if (!imageDirectory.exists()) {
            if (imageDirectory.mkdirs()) {
                System.out.println("Carpeta de imágenes creada exitosamente");
            } else {
                System.out.println("No se pudo crear la carpeta de imágenes");
            }
        }
    	
    	
        try {
        	// Genera un nombre de archivo único
            String fileName = UUID.randomUUID().toString(); 
//            System.out.println("Nombre:    "+fileName);
//            String filePath = UPLOAD_DIR + "/" + fileName;
//            
//            // Guarda el archivo en el directorio especificado
//            Files.write(Paths.get(filePath), image.getBytes());

            
            // --------

            // Obtén la extensión del archivo original
            String originalFileName = StringUtils.cleanPath(image.getOriginalFilename());
            String fileExtension = StringUtils.getFilenameExtension(originalFileName);

            // Construye el nombre de archivo completo con la extensión
            String fullFileName = fileName + "." + fileExtension;

            // Construye la ruta completa del archivo en el servidor
            String filePath = UPLOAD_DIR + "/" + fullFileName;

            Files.write(Paths.get(filePath), image.getBytes());
            // ----------
            
            
            return ResponseEntity.ok(fullFileName);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error al guardar la imagen");
        }
    }


	
}
