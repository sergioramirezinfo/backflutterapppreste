package com.example.backPreste.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.backPreste.entity.solicitud;
import com.example.backPreste.entity.usuario;
import com.example.backPreste.service.solicitudService;
import com.example.backPreste.service.usuarioService;

@RestController
@RequestMapping("/solicitud")
@CrossOrigin
public class solicitudController {
	@Autowired
	private solicitudService solicitudxService;
	
	
	@GetMapping
	public List<solicitud> listar(){
		return solicitudxService.listar();
	}
	
	@PostMapping
	public solicitud insertar(@RequestBody solicitud sol){
		return solicitudxService.insertar(sol);
	}
	
	@DeleteMapping
	public void eliminar(@RequestBody int id){
		solicitudxService.eliminar(id);
	}
	
	@Autowired
	private usuarioService usuarioxService;
	@RequestMapping(value = "listarUx/{usx}", method = RequestMethod.GET)
	public List<solicitud> listarSolicitudesUx(@PathVariable int usx) {
		usuario usuariox = new usuario();
		usuariox.setId(usx);
//	    usuario usuario = usuarioService.getUsuarioById(usx);
	    return solicitudxService.listarSolicitudesUx(usuariox);
	}


	
}
