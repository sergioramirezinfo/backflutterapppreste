package com.example.backPreste.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.backPreste.entity.usuario;
import com.example.backPreste.service.usuarioService;

@RestController
@RequestMapping("/usuario")
@CrossOrigin
public class usuarioController {
	@Autowired
	private usuarioService usuarioxService;
	
	
	@GetMapping
	public List<usuario> listar(){
		return usuarioxService.listar();
	}
	
	@PostMapping
	public usuario insertar(@RequestBody usuario us){
		return usuarioxService.insertar(us);
	}
	
	@DeleteMapping
	public void eliminar(@RequestBody int id){
		usuarioxService.eliminar(id);
	}
}
