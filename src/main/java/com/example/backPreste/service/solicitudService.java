package com.example.backPreste.service;

import java.util.List;

import com.example.backPreste.entity.solicitud;
import com.example.backPreste.entity.usuario;

public interface solicitudService {

	public abstract solicitud insertar(solicitud sol);	
	
	public abstract List<solicitud> listar();
	public abstract void eliminar(int id);
	public abstract List<solicitud> listarSolicitudesUx(int usx);

	List<solicitud> listarSolicitudesUx(usuario usx);
	
	
}
