package com.example.backPreste.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.backPreste.entity.respaldo;
import com.example.backPreste.entity.solicitud;
import com.example.backPreste.repo.respaldoRepo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Service
public class respaldoServiceImplement implements respaldoService{

	@PersistenceContext
    private EntityManager entityManager;
	
	@Autowired
	private respaldoRepo respaldoxRepo;
	
	
	public respaldo insertar(respaldo resp) {
		return respaldoxRepo.save(resp);
	}
	
	public List<respaldo> listar() {
		return respaldoxRepo.findAll();
	}
	
	public void eliminar(int id) {
		respaldoxRepo.deleteById(id);
	}

	@Override
	public List<respaldo> listarRespaldosSx(solicitud usx) {
		String query = "SELECT x FROM respaldo x WHERE x.solicitud.id = :usx";
	    List<respaldo> lista = entityManager.createQuery(query)
	            .setParameter("usx", usx.getId())
	            .getResultList();
	    if (lista.isEmpty()) {
	        return null;
	    }
	    return lista;
	}

	@Override
	public List<respaldo> listarRespaldosSx(int usx) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	
	
	
}
