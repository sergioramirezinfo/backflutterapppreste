package com.example.backPreste.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.backPreste.entity.solicitud;
import com.example.backPreste.entity.usuario;
import com.example.backPreste.repo.solicitudRepo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Service
public class solicitudServiceImplement implements solicitudService{

	
	@PersistenceContext
    private EntityManager entityManager;
	@Autowired
	private solicitudRepo solicitudxRepo;
	
	
	public solicitud insertar(solicitud sol) {
		return solicitudxRepo.save(sol);
	}
	
	public List<solicitud> listar() {
		return solicitudxRepo.findAll();
	}
	
	public void eliminar(int id) {
		solicitudxRepo.deleteById(id);
	}

	@Override
	public List<solicitud> listarSolicitudesUx(usuario usx) {
		String query = "SELECT x FROM solicitud x WHERE x.usuario.id = :usx";
	    List<solicitud> lista = entityManager.createQuery(query)
	            .setParameter("usx", usx.getId())
	            .getResultList();
	    if (lista.isEmpty()) {
	        return null;
	    }
	    return lista;
	}

	@Override
	public List<solicitud> listarSolicitudesUx(int usx) {
		// TODO Auto-generated method stub
		return null;
	}
}
