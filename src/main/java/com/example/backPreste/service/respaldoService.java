package com.example.backPreste.service;

import java.util.List;

import com.example.backPreste.entity.respaldo;
import com.example.backPreste.entity.solicitud;

public interface respaldoService {

	public abstract respaldo insertar(respaldo resp);

	public abstract List<respaldo> listar();

	public abstract void eliminar(int id);

	public abstract List<respaldo> listarRespaldosSx(int solx);

	List<respaldo> listarRespaldosSx(solicitud solx);

}
