package com.example.backPreste.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.backPreste.entity.usuario;
import com.example.backPreste.repo.usuarioRepo;

@Service
public class usuarioServiceImplement implements usuarioService{

	@Autowired
	private usuarioRepo usuariosRepo;
	
	
	public usuario insertar(usuario us) {
		return usuariosRepo.save(us);
	}
	
	public List<usuario> listar() {
		return usuariosRepo.findAll();
	}
	
	public void eliminar(int id) {
		usuariosRepo.deleteById(id);
	}

	public usuario getUsuarioById(Integer usx) {
	    Optional<usuario> optionalUsuario = usuariosRepo.findById(usx);
	    return optionalUsuario.orElse(null);
	}

}

