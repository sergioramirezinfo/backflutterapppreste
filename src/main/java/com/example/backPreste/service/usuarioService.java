package com.example.backPreste.service;

import java.util.List;

import com.example.backPreste.entity.usuario;

public interface usuarioService {

	public abstract usuario insertar(usuario us);	
	
	
	public abstract List<usuario> listar();
	
	public abstract void eliminar(int id);


	public abstract usuario getUsuarioById(Integer usx);
	
}