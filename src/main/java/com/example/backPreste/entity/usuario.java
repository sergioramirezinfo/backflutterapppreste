package com.example.backPreste.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;

@Entity
public class usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "ci")
    private String ci;
    
    @Column(name = "cel")
    private String cel;
    
    // Constructor(es)
    public usuario() {
        // Constructor vacío
    }
    
    public usuario(String ci, String cel) {
        this.ci = ci;
        this.cel = cel;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public String getCel() {
		return cel;
	}

	public void setCel(String cel) {
		this.cel = cel;
	}
    
    // Métodos getter y setter
    
}
