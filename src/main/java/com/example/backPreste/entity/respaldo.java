package com.example.backPreste.entity;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.persistence.ManyToOne;

@Entity
public class respaldo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "tipo")
    private String tipo;
    
    @Column(name = "imagen", columnDefinition = "TEXT")
    private String imagen;

    
    @Column(name = "ocr")
    private String ocr;
    
    @ManyToOne
    private solicitud solicitud;
    
    // Constructor, getters, and setters
    public respaldo() {
    }
    
    // Constructor con atributos
    public respaldo(String tipo, String imagen, String ocr) {
        this.tipo = tipo;
        this.imagen = imagen;
        this.ocr = ocr;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getOcr() {
		return ocr;
	}

	public void setOcr(String ocr) {
		this.ocr = ocr;
	}

	public solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(solicitud solicitud) {
		this.solicitud = solicitud;
	}
    
}
