package com.example.backPreste.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Column;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.JoinColumn;

@Entity
public class solicitud {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @Column(name = "monto")
    private long monto;
    
    @Column(name = "proposito")
    private String proposito;
    
    @Column(name = "moneda")
    private String moneda;
    
    @Column(name = "ingreso_mensual")
    private long ingresoMensual;
    
    @Column(name = "estado_civil")
    private String estadoCivil;
    
    @Column(name = "tipo_ingreso")
    private String tipoIngreso;
    
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private usuario usuario;
    
    // Constructor(es)
    public solicitud() {
        // Constructor vacío
    }
    
    public solicitud(long monto, String proposito, String moneda, long ingresoMensual, String estadoCivil, String tipoIngreso) {
        this.monto = monto;
        this.proposito = proposito;
        this.moneda = moneda;
        this.ingresoMensual = ingresoMensual;
        this.estadoCivil = estadoCivil;
        this.tipoIngreso = tipoIngreso;
    }
    
    // Métodos getter y setter
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public long getMonto() {
        return monto;
    }
    
    public void setMonto(long monto) {
        this.monto = monto;
    }
    
    public String getProposito() {
        return proposito;
    }
    
    public void setProposito(String proposito) {
        this.proposito = proposito;
    }
    
    public String getMoneda() {
        return moneda;
    }
    
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
    
    public long getIngresoMensual() {
        return ingresoMensual;
    }
    
    public void setIngresoMensual(long ingresoMensual) {
        this.ingresoMensual = ingresoMensual;
    }
    
    public String getEstadoCivil() {
        return estadoCivil;
    }
    
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }
    
    public String getTipoIngreso() {
        return tipoIngreso;
    }
    
    public void setTipoIngreso(String tipoIngreso) {
        this.tipoIngreso = tipoIngreso;
    }
    
    public usuario getUsuario() {
        return usuario;
    }
    
    public void setUsuario(usuario usuario) {
        this.usuario = usuario;
    }
}
