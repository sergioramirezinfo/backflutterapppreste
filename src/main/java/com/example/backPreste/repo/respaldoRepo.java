package com.example.backPreste.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.backPreste.entity.respaldo;
import com.example.backPreste.entity.usuario;

@Repository
public interface respaldoRepo extends JpaRepository<respaldo, Integer> {

}
