package com.example.backPreste.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.backPreste.entity.usuario;

@Repository
public interface usuarioRepo extends JpaRepository<usuario, Integer> {

}
